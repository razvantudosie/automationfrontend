npm iun
# Automation - End to End Tests

![dpdgroup](https://www.dpd.com/extension/dpd_portaldesign/design/dpd_portal/images/dpdgrouplogo.png)

### Useful links

### Getting Started

Install [Node.js](https://nodejs.org/en/) and then clone this repository:
```sh
$ git clone <this_repo>
$ cd <this_repo>
$ npm install
```

### Running Tests
Running all the tests
```sh
$ npm run test
```
Running test groups
```sh
$ npm run group login
```
Skipping test groups
```sh
$ npm run skipgroup login
```
Running test on specific browser
```sh
$ npm test [browser name]
```

[View more custom commands](http://nightwatchjs.org/guide#running-tests)


### Page Objects

Page objects should have the following folder structure:

=> pages/`page_name`/`page_object_name`

e.g `pages`/`google_home_page`/`googleHome.js`


#### Creating Page Objects

Elements should be defined in the **elements object**

```js
// Selectors
var elements = {
  txtUsername      : { selector: 'input[name="username"]'},
  txtPassword      : { selector: 'input[name="password"]'},
  btnLogin         : { selector: '//button[@name="login"]', locateStrategy: 'xpath'}
};
```
** Note: ** Each selector should be prefixed with the following:

1. `lbl` - label
2. `btn` - button
3. `txt` - textbox
4. `tbl` - table

Anchors and other elements do not need to be prefixed.

#### Finding Selectors

```js
// very bad
var elements = {
  txtUsername : { selector: './/*[@id="weight"]/div/div[1]/div[2]/div[3]/div[1]/h2', locateStrategy:'xpath'}
};
```

```js
// good
var elements = {
   btnLogin : { selector: '//button[@name="login"]', locateStrategy: 'xpath'} ,
};
```

** Note: ** CSS can be used, even though XPath was used in the example above:

#### Commands

Commands should be created similarly to the example below. Each command should have
preceding comments that contain the `@params`, `@returns` as well as a `description`
of how the command should function.

```js

var commands = {};

/**
 * Login with a user
 * @param  {Boolean} isValid Specifies wether to login with a valid or invalid user
 */
commands.login = function(isValid){
    // super awesome code goes here
 }
```

Where applicable, use `sections` to easily provide element-level nesting. For example for to represent a complex parent-child relationship.(e.g menus)

An example of how `sections` can be used:

```js
module.exports = {
  sections: {
    menu: {
      selector: '#menu',
      elements: {
        change.password: {
          selector: '#change_password'
        },
        logout: {
          selector: '#logout'
        }
      }
    }
  }
};
```

### Data Driven Testing

Data will be stored in JSON or JS files within `project_name/data` folder. Each file should be specific to particular page object / test suite.

An example of how to configure and use data from `globals.json`:

```js

// nightwatch.json
"globals_path" : "lib/data/globals.js",

// globals in test
module.exports = {

  'My Test' : function(client) {
    var password = client.globals.password
  }
}

// globals in page objects
commands.setPassword = function(){
  this.waitForElementVisible('@txtPassword', this.api.globals.password)
}
```

** Note: ** Only objects stored in the `globals.json` can be used in this way. Other JSON or JS files would have to be `require`d in order to be used in tests and page objects.

### Reporting

After each automation run, the test results will be pushed to the Nightwatcher reporter. The graph that Nightwatcher generates will then be inserted into this README. This will provide the team with an overview of the overall progress of the automation.

#### Recording nightwatcher results

```sh
$ npm run report //runs all tests and pushes results to nightwatcher
```

#### Viewing the reports

1. Click [here](http://nightwatcher.nodeqa.io/login) to view the report.
2. Login with your bitbucket account.
3. Select the **repository menu** option in the left navigation.
4. Click on the **details button** for **qualityworkscgdpd/project-odf** repo.
5. Click on the **add to watchlist button**.
6. Return to the dashboard using the left menu.
7. To the right, select the **qualityworkscgdpd/project-odf** repo from the **current project** dropdown list.
8. Reports should now be available.

An example of a Nightwatcher report is shown below:

![Nightwatcher Report](https://res.cloudinary.com/dvvgdt5sz/image/upload/v1461433138/Screen_Shot_2016-04-23_at_12.37.53_PM_ojptyq.png)
