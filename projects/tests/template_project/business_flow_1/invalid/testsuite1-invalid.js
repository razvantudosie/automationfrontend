"use strict";

var googleHome;

module.exports = {

    before: function ( client ) {
        client.maximizeWindow();

        googleHome = client.page.template_project.google_home_page.googleHome();
        googleHome.navigate();
    },

    "Negative - Search DPD": function ( client ) {
        googleHome.searchNegative();
    },



    after: function ( client ) {
        client.end()
    }

};
