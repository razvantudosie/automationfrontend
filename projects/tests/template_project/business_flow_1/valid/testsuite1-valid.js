"use strict";
var path = require('path');
var async = require('async');

var googleHome;

module.exports = {

    before: function ( client , done ) {

        var mysqlData = path.join(__dirname , "../../../../data/template_project/business_flow_1/import/MySql.json");

        async.series([
            // MySQL Import
            function(callback){
                mySQLImport(mysqlData, "aaa", "bb", callback);
            },
            // Maximize Window and navigate to URL
            function(callback){
                client.maximizeWindow();
                googleHome = client.page.template_project.google_home_page.googleHome();
                googleHome.navigate();
                callback();
            }
        ], function (err) {
            if(err) {
                console.log("ERROR : " ,err);
                // Nightwatchjs bug (Intentionally fail the test from the hook):
                // https://github.com/nightwatchjs/nightwatch/pull/1134
                done(new Error('"ERROR : " ,err'));
            } else {
                console.log("Done!");
                done();
            }
        });

        // client.maximizeWindow();
        // googleHome = client.page.googleHome();
        // googleHome.navigate();

    },

    "Valid - Search DPD": function ( client ) {
        googleHome.searchValid();
    },


    after: function ( client ) {
        client.end()
    }

};
