"use strict";
var path = require('path');
var async = require('async');
var mySQLImport = require(path.join(__dirname , "../../../../../node_modules/automation-js-common/lib/tools/import/MySQLImport.js"));
var mysqlData = path.join(__dirname , "../../../data/business_flow_1/Import/MySql.json");
var floorManagement;

module.exports = {

    before: function ( client , done) {


        console.log("Data = ", mysqlData);

        var mysqlData = path.join(__dirname , "../../../../data/quantum/business_flow_1/Import/MySql.json");

        async.series([
            // MySQL Import
            function(callback){
                mySQLImport(mysqlData, "aaa", "bb", callback);
            },
            // Maximize Window and navigate to URL
            function(callback){
                client.maximizeWindow();

                floorManagement = client.page.quantum.floor_management_page.floorManagement();
                floorManagement.navigate();
                callback();
            }
        ], function (err) {
            if(err) {
                console.log("ERROR : " ,err);
                //done(new Error('"ERROR : " ,err'));
            } else {
                console.log("Done!");
                done();
            }
        });
    },

    "Valid - Check Elements": function (client) {
        floorManagement.checkElements();
    },


    after: function ( client ) {
        client.end();
    }

};
