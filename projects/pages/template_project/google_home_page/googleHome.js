/**
 * Created by razvan.tudosie on 30/09/16.
 */

"use strict";

var testData_valid = require("../../../data/template_project/business_flow_1/Valid/testData-Valid.js");
var testData_invalid = require("../../../data/template_project/business_flow_1/Invalid/testData-Invalid.js");
var data  = require("../../../globals");
const PAUSE = data.TEST_PAUSE;

// Page Selectors
var elements = {
    txtSearch           : { selector: 'input[type=text]'},
    btnSearch           : { selector: './/*[@name="btnG"]' ,  locateStrategy: 'xpath'},
    dpdResults          : { selector: './/div[text()="Dynamic Parcel Distribution"]' ,  locateStrategy: 'xpath'},
    negativeSearch      : { selector: './/li[text()="Make sure that all words are spelled correctly."]' ,  locateStrategy: 'xpath'},
};

var commands = {};

/**
 * Verify that a user is unable to click the login button if they have not entered a username
 */
commands.searchValid = function(){

    var browser = this;

    browser.api.pause(PAUSE);

    this.waitForElementVisible('@txtSearch',"Verify txtSearch is visible")
        .click('@txtSearch')
        .setValue('@txtSearch', testData_valid.valid.searchTxt)
        .waitForElementVisible('@txtSearch', 'Verify input search is visible')
        .click('@btnSearch')
        .waitForElementVisible('@dpdResults','Verify "Dynamic Parcel Distribution" appeared on the first page')
},

commands.searchNegative = function(){

    var browser = this;

    browser.api.pause(PAUSE);

    this.waitForElementVisible('@txtSearch',"Verify txtSearch is visible")
        .click('@txtSearch')
        .setValue('@txtSearch', testData_invalid.invalid.searchTxt)
        .waitForElementVisible('@txtSearch', 'Verify input search is visible')
        .click('@btnSearch')
        .waitForElementVisible('@negativeSearch','Verify "Make sure that all words are spelled correctly" appeared on the first page')
},

module.exports = {
    url      : data.URL,
    commands : [commands],
    elements : elements
};
