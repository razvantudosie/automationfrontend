/**
 * Created by razvan.tudosie on 30/09/16.
 */

"use strict";

var testData_valid = require("../../../data/quantum/business_flow_1/Valid/testData-Valid.js");
var testData_invalid = require("../../../data/quantum/business_flow_1/Invalid/testData-Invalid.js");
var data  = require("../../../data/quantum/globals");
const PAUSE = data.TEST_PAUSE;

// Page Selectors
var elements = {
    menuLnk           : { selector: './/*[@class="menu-link"]', locateStrategy: 'xpath'},
    filtersPnl        : { selector: './/*[@class="content-box route-filters"]',  locateStrategy: 'xpath'},
    routeActivityWid  : { selector: './/*[@class="content-box route-activity"]',  locateStrategy: 'xpath'},
    feedActivtyWid  : { selector: './/*[@class="content-box activity-feed"]',  locateStrategy: 'xpath'},
    plannedParcelsWid  : { selector: './/*[@id="web_components"]/div[2]/div/div[2]/div[2]/div[2]/div',  locateStrategy: 'xpath'},
    loadingStatusWid : { selector: './/*[@id="web_components"]/div[2]/div/div[2]/div[2]/div[3]/div',  locateStrategy: 'xpath'},
    driverStatusWid : { selector: './/*[@id="web_components"]/div[2]/div/div[2]/div[2]/div[4]/div',  locateStrategy: 'xpath'},

};

var commands = {};

/**
 * Verify the widgets in the page
 */
commands.checkElements = function(){

    var browser = this;

    browser.api.pause(PAUSE);

    this.waitForElementVisible('@menuLnk',"Verify menu is visible")
        .click('@menuLnk');
    this.waitForElementVisible('@filtersPnl',"Verify that filter widget is visible");
    this.waitForElementVisible('@routeActivityWid',"Verify route activity feed widget is visible");
    this.waitForElementVisible('@plannedParcelsWid',"Verify loading status widget is visible");
    this.waitForElementVisible('@loadingStatusWid',"Verify loading status widget is visible");
    this.waitForElementVisible('@driverStatusWid',"Verify driver status widget is visible");
},

    /**
     * Verify the data in the page , after a mysql import
     */
  commands.checkData = function(){

    var browser = this;

    browser.api.pause(PAUSE);

    this.waitForElementVisible('@txtSearch',"Verify txtSearch is visible")
        .click('@txtSearch')
        .setValue('@txtSearch', testData_invalid.invalid.searchTxt)
        .waitForElementVisible('@txtSearch', 'Verify input search is visible')
        .click('@btnSearch')
        .waitForElementVisible('@negativeSearch','Verify "Make sure that all words are spelled correctly" appeared on the first page')
},

module.exports = {
    url      : data.URL,
    commands : [commands],
    elements : elements
};
