/*
   * Here you can add all global variables specific for your project.
   * Please take a look to the following example.
   */

var path = require('path');
var dotenv = require('dotenv');

// Load .env file:
var envFile = path.join(__dirname, "../default.env");
dotenv.config({path: envFile});

// Globals:
global.mySQLImport = require(path.join(__dirname , "../node_modules/automation-js-common/lib/tools/import/MySQLImport.js"));


module.exports = {

  /*
  * This is the URL for the home page
  */
  URL: 'https://www.google.com',

 /*
  * This controls whether to abort the test execution when an assertion failed and skip the rest.
  * It's being used in waitFor commands and expect assertions {nightwatch setting}
  */
  abortOnAssertionFailure : false,

 /*
  * Default timeout value in milliseconds for waitFor commands and implicit waitFor value for
  * expect assertions. Time in ms {nightwatch setting}
  *
  */
  waitForConditionTimeout : 10000,

 /*
  * Timeout value that can be easily accessed in tests and page objects. Time in ms
  * {health dialog setting}
  */
  TIMEOUT : 10000,

 /*
  * Pause value that is used in loops that contain commands. When commands are repetively
  * performed in loops, a pause is sometimes needed. Time in ms. {health dialog setting}
  */
  LOOP_PAUSE: 500,

  /**
   *  Pause that is used inside tests. Time in ms. {health dialog setting}
   */

  TEST_PAUSE: 2000,

  /*
   * Escape characters for useful KEYS
   */
  KEYS: {
    BACKSPACE: "\b"
  }

};
